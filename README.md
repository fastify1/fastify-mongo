This project was bootstrapped with Fastify-CLI.

## Technical requirements

- Node.js
- A text editor to try the example code
- Docker
- An HTTP client to test out code, such as CURL or Postman

## Available Scripts

In the project directory, you can run:

### `npm run mongo:start`

To start a Mongo DB Docker container

### `npm run mongo:stop`

To stop Mongo DB Docker container

### `npm run dev`

To start the app in dev mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm start`

For production mode


## Testing

You can run the automated tests with the following command: 

### `npm run test`

Or you can manually test the endpoints using the **test.http** file (have in mind to update the todo's id and JWT accordingly).

