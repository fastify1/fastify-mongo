module.exports = {
  level: process.env.LOG_LEVEL,
  redact: {
    censor: '***',
    paths: [
      'req.headers.authorization',
      'req.body.password',
      'req.body.email'
    ]
  },
  serializers: {
    req: function (request) {
      // const logBody = request.context.config.logBody // add config: { logBody: true } to routes, that we want to log
      return {
        method: request.method,
        url: request.url,
        routeUrl: request.routerPath,
        version: request.headers?.['accept-version'],
        user: request.user?.id,
        headers: request.headers, // here specify which headers to log, sensitive information
        body: request.body, // amy be too big for logging
        hostname: request.hostname,
        remoteAddress: request.ip,
        remotePort: request.socket?.remotePort
      }
    },
    res: function (reply) {
      return {
        statusCode: reply.statusCode,
        responseTime: reply.getResponseTime()
      }
    }
  }
}
