const loggerOptions = require('./logger-options')

module.exports = {
  ajv: {
    customOptions: {
      removeAdditional: 'all'
    }
  },
  disableRequestLogging: true,
  logger: loggerOptions
}
