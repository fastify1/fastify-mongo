const t = require('tap')
const { buildApp } = require('./helper')

t.test('the application should start', async (t) => {
  const app = await buildApp(t)
  await app.ready()
  t.pass('the application is ready')
})

t.test('the alive route is online', async (t) => {
  const app = await buildApp(t)
  const response = await app.inject({
    method: 'GET',
    url: '/'
  })
  t.same(response.json(), { root: true })
})

t.test('the application should not start', async (mainTest) => {
  mainTest.test('if there are missing ENV variables', async (t) => {
    try {
      await buildApp(t, {
        NODE_ENV: 'test',
        MONGO_URL: undefined
      })
      t.fail('the server should npt start')
    } catch (error) {
      t.ok(error, 'error must be set')
      t.match(error.message, "required property 'MONGO_URL'")
    }
  })

  mainTest.test('when mongodb is un reachable', async (t) => {
    try {
      await buildApp(t, {
        NODE_ENV: 'test',
        MONGO_URL: 'mongodb://localhost:7777/test'
      })
      t.fail('the server should not start')
    } catch (error) {
      t.ok(error, 'error must be set')
      t.match(error.message, 'connect ECONNREFUSED')
    }
  })
})

t.test('cannot access protected route', async (t) => {
  const app = await buildApp(t)
  const privateRoutes = ['/todos', '/todos/654b838b17f40e245cf65e6d']
  for (const url of privateRoutes) {
    const response = await app.inject({ method: 'GET', url })
    t.equal(response.statusCode, 401)
    t.match(response.json(), {
      statusCode: 401,
      error: 'Unauthorized',
      message: 'No Authorization was found in request.headers'
    })
  }
})

t.test('register user', async (mainTest) => {
  mainTest.test('register new user', async (t) => {
    const app = await buildApp(t)
    const response = await app.inject({
      method: 'POST',
      url: '/register',
      body: {
        username: 'test',
        password: '123456'
      }
    })
    t.equal(response.statusCode, 201)
    t.match(response.json(), { registered: true })
  })

  mainTest.test('register existing user', async (t) => {
    const app = await buildApp(t)
    const response = await app.inject({
      method: 'POST',
      url: '/register',
      body: {
        username: 'test',
        password: '234567'
      }
    })
    t.equal(response.statusCode, 409)
    t.match(response.json(), { statusCode: 409, error: 'Conflict', message: 'User already registered' })
  })
})

t.test('login user', async (mainTest) => {
  mainTest.test('successful login', async (t) => {
    const app = await buildApp(t)
    const login = await app.inject({
      method: 'POST',
      url: '/authenticate',
      body: {
        username: 'test',
        password: '123456'
      }
    })
    t.equal(login.statusCode, 200)
    t.match(login.json(), { token: /(\w*\.){2}.*/ })

    t.test('access protected route', async (t) => {
      const response = await app.inject({
        method: 'GET',
        url: '/me',
        headers: {
          Authorization: `Bearer ${login.json().token}`
        }
      })
      t.equal(response.statusCode, 200)
      t.match(response.json(), { username: 'test' })
    })
  })

  mainTest.test('not successful login', async (t) => {
    const app = await buildApp(t)
    const response = await app.inject({
      method: 'POST',
      url: '/authenticate',
      body: {
        username: 'test',
        password: 'wrong'
      }
    })
    t.equal(response.statusCode, 500)
    t.match(response.json(), { message: 'Wrong credentials provided' })
  })
})
