const fcli = require('fastify-cli/helper')

const startArgs = '-l info --options app.js'
const envParam = {
  NODE_ENV: 'test',
  MONGO_URL: 'mongodb://localhost:27017/test'
}

// Fill in this config with all the configurations
// needed for testing the application
function config (env) {
  return {
    configData: env
  }
}

async function buildApp (t, env = envParam, serverOptions) {
  const app = await fcli.build(startArgs,
    config({ ...envParam, ...env }),
    serverOptions
  )
  t.teardown(() => app.close())
  return app
}

module.exports = {
  buildApp
}
