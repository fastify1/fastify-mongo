const t = require('tap')
const fs = require('fs').promises

t.test('test a primitive', t => {
  t.plan(2)
  const myVar = '42'
  t.equal(myVar, '42', 'this string is 42')
  t.equal(typeof myVar, 'string', 'type of myVar')
})

t.test('test a reference', t => {
  t.plan(2)
  const obj = { hello: 'world' }
  t.strictSame(obj, { hello: 'world' }, 'objects are identical')
  t.match(obj, { hello: 'world' }, 'objects are similar')
})

t.test('sub test', function testFunction (t) {
  t.plan(2)
  const fastifyValue = null
  t.notOk(fastifyValue, 'a falsy value')
  t.test('boolean assertions', subTapTest => {
    subTapTest.plan(1)
    subTapTest.ok(true, 'true is ok')
  })
})

t.test('async test', async t => {
  const fileContent = await fs.readFile('./package.json', 'utf8')
  t.type(fileContent, 'string', 'file content is string')

  await t.test('check main file', async subTest => {
    const json = JSON.parse(fileContent)
    subTest.match(json, { version: '1.0.0' })
    const appIndex = await fs.readFile(json.main, 'utf8')
    subTest.match(appIndex, 'use strict', 'the main file is correct')
  })
  t.pass('test completed')
})
